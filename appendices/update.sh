#!/usr/bin/env bash

rm rtp.txt
cp ../../improving-protocol-standards/src/examples/extended_diagrams/rtp.txt .

rm udp.txt
cp ../../improving-protocol-standards/src/examples/extended_diagrams/udp.txt .

#rm rfc2xml.md
#rm rfc2xml.tex
#rm rfc2xml.pdf
#cp ../../improving-protocol-standards/src/input_parsers/extended_diagrams/README.md rfc2xml.md
#pandoc rfc2xml.md -f markdown -t latex -s -o rfc2xml.tex --variable papersize=a4paper -V fontsize=17pt --listings -H pandoc-setup.tex
#pdflatex rfc2xml.tex

#rm extended_diagrams.md
#rm extended_diagrams.tex
#rm extended_diagrams.pdf
#cp ../../improving-protocol-standards/src/rfc2xml/README.md extended_diagrams.md
#pandoc extended_diagrams.md -f markdown -t latex -s -o extended_diagrams.tex --variable papersize=a4paper -V fontsize=17pt --listings -H pandoc-setup.tex
#pdflatex extended_diagrams.tex