l4proj.pdf: l4proj.tex l4proj.bib
	pdflatex l4proj.tex
	bibtex l4proj
	pdflatex l4proj.tex
	pdflatex l4proj.tex

clean:
	rm l4proj.aux l4proj.bbl l4proj.blg l4proj.log l4proj.out l4proj.toc l4proj.pdf 2> /dev/null || true

